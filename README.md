# Gitlab Trello Power-up

See TODO URL ABOUT for informations about the Power-Up (or write it as .MD and include here, must also be in /about)
Gitlab access_token must have permission "read_api"

https://trello-gitlab.pages.dev/

## TODO

- [X] Add branches and merge requests containing card id
- [X] Optimize fetch commits
  - [X] Fetch less when branch contains card id
  - [X] Add button to fetch more commits (more pages and items)
- [X] Add regex parameter to identify branches & commits
- [ ] Add option to attach manually a branch or merge request
- [ ] Make a nice /about page explaining configuration and usage
- [X] ~~Create CI file to upload on Gitlab pages~~ Deployed on Cloudflare Pages
- [ ] Submit to Trello Power-ups Marketplace ? (all free) 

## Get started

- `npm run dev` : to start application in development mode
- `npm run build` : to build production files in `/dist`

## Vue 3 + TypeScript + Vite

Project scaffolded from [vitejs.dev](https://vitejs.dev/guide/), template :  [vue-ts](https://vite.new/vue-ts)

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- Just use [Gitpod](https://www.gitpod.io/) workspace integrated in [Gitlab](https://gitlab.com/)
- or [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)