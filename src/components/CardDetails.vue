<script setup lang="ts">

import { BranchSchema, CommitSchema, DiscussionNoteSchema, MergeRequestSchema } from '@gitbeaker/rest';
import { GitlabRepositoryInfo, getGitlabApi, trello, getTrelloData, BranchesByRepo, TrelloData, regexCardId, MergeRequestInfo } from '../helpers';
import { Ref, ref } from 'vue';

/**
 * Trello Power up library
 */
const t = trello.iframe();

/**
 * Trello card id 
 */
let cardId: string;

/**
 * Regex to filter branches & commits for card id
 */
let cardRegex: RegExp;

/**
 * Repo infos from Trello data
 */
let repoInfos: Ref<GitlabRepositoryInfo[]> = ref([]);

/**
 * Gitlab resources grouped by repo infos
 */
let branchesByRepo: Ref<BranchesByRepo[]> = ref([]);

/**
 * Gitlab branches for this card
 */
let branches: Ref<(BranchSchema & { repoName: string })[]> = ref([]);

/**
 * Gitlab merge requests for this card
 */
let mergeRequests: Ref<MergeRequestInfo[]> = ref([]);

/**
 * Gitlab commits for this card
 */
let commits: Ref<(CommitSchema & { repoName: string })[]> = ref([]);

/**
 * Pagination to fetch older commits
 */
let page = 1;

/**
 * True to show loader while initializing values
 */
let isLoading = ref(true);

/**
 * Trello data
 */
let data: Ref<TrelloData>;

// Get Trello data from board
getTrelloData().then(async d => {
    data = ref(d);

    // Get repositories information from trello data
    repoInfos.value = data.value?.repoInfos;

    if (repoInfos.value) {
        // Get id 
        const card = await t.card("idShort")
        // Short id used in commits and branches to identify card
        cardId = card.idShort;
        cardRegex = new RegExp(data.value.regex.replace(regexCardId, cardId));
        // Fetch gitlab resources
        await refresh();
    } else {
        // Just remove loading screen
        isLoading.value = false;
    }
}, () => isLoading.value = false);

/**
 * Get branches containing card id
 */
async function fetchBranches(): Promise<void> {
    // Iterate repo info to get branches from all repos
    for (const repoInfo of repoInfos.value) {
        const api = getGitlabApi(repoInfo);
        // Get branches
        const newBranches = await api.Branches.all(repoInfo.repoId, {
            // Replace regex card id identifier w/ current card id
            regex: cardRegex.source
        });
        // Add them to all branches array
        Array.prototype.push.apply(branches.value, newBranches.map(branch => ({ ...branch, ...{ repoName: repoInfo.repoName } })));
        // Add them to branches by repo array
        let repo = branchesByRepo.value.find(context => context.repoInfo.repoId === repoInfo.repoId);
        if (!repo) {
            // Create new item w/ searched repo info
            repo = { repoInfo, branches: newBranches };
            branchesByRepo.value.push(repo);
        } else {
            // Replace repo branches
            repo.branches = newBranches;
        }
    }
}

/**
 * Get merge requests associated to branches fetched
 */
async function fetchMergeRequests(): Promise<void> {
    // Iterate repo contexts to get already fetched branches from all repos
    for (const repo of branchesByRepo.value) {
        const repoInfo = repo.repoInfo;
        const api = getGitlabApi(repoInfo);
        let repoMergeRequests: MergeRequestSchema[] = [];
        // Iterate repo branches
        for (const branch of repo.branches) {
            // Get merges request w/ this source branch
            const newMergeRequests = await api.MergeRequests.all({ projectId: repoInfo.repoId, sourceBranch: branch.name });
            repoMergeRequests = [...repoMergeRequests, ...newMergeRequests];
        }
        // No branches, get merge requests containing card id 
        if (!repo.branches.length) {
            const newMergeRequests = await api.MergeRequests.all({ projectId: repoInfo.repoId, search: cardId });
            repoMergeRequests = [...repoMergeRequests, ...newMergeRequests];
        }
        // Fetch more informations
        for (const mergeRequest of repoMergeRequests) {
            // Fetch approvals
            const approvals = await api.MergeRequestApprovals
                .showConfiguration(repoInfo.repoId, { mergerequestIId: mergeRequest.iid });
            // Fetch unresolved comments
            const unresolvedNotes = (await api.MergeRequestDiscussions.all(repoInfo.repoId, mergeRequest.iid))
                .filter(discussion => !!discussion.notes)
                .flatMap(discussion => <DiscussionNoteSchema[]>discussion.notes)
                .filter(note => !!note.resolvable && !note.resolved && note.type === 'DiffNote')
                .map(note => ({ ...note, url: `${mergeRequest.web_url}#note_${note.id}` }));
            // Fetch diverged commits
            const nbCommitsBehindTarget = <number>(await api.MergeRequests
                .show(repoInfo.repoId, mergeRequest.iid, { includeDivergedCommitsCount: true }))
                .diverged_commits_count;
            // Add MR to all merge requests array
            mergeRequests.value.push({
                ...mergeRequest,
                ...{
                    repoName: repoInfo.repoName,
                    approved: !!approvals.approved_by?.length,
                    notes: unresolvedNotes,
                    nbCommitsBehindTarget
                }
            });
        }
    }
}

/**
 * Fetch commits from branches containing card id or if none, fetch all commits
 * Then filter commits containing card id in message
 */
async function fetchCommits(): Promise<void> {
    // Iterate repositories to get branches and commits containing id
    for (const repo of branchesByRepo.value) {
        const repoInfo = repo.repoInfo;
        const api = getGitlabApi(repoInfo);
        // Iterate branches to get commits from
        for (const branch of repo.branches) {
            // Search function from Gitlab API is not enabled for non premium Gitlab instance
            // So we get all commits from branches containing card id
            const newCommits = await api.Commits.all(repoInfo.repoId, {
                refName: branch.name,
                perPage: data.value.perPage, page: page
            });
            await fillCommits(newCommits, repoInfo, branch);
        }
        // If no branches, get all commits
        if (!repo.branches.length) {
            const newCommits = await api.Commits.all(repoInfo.repoId, {
                perPage: data.value.perPage, page: page
            });
            await fillCommits(newCommits, repoInfo);
        }
    }

    // Sorts commits w/ the most recent first
    commits.value.sort(function (a, b) {
        return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
    });
}

/**
 * Iterate commits to push only those containing card short id
 * If missing branch ref, request API for refs
 * @param newCommits new commits to push into after filtering
 * @param repoInfo info from Trello data
 * @param branch 
 */
async function fillCommits(newCommits: CommitSchema[], repoInfo: GitlabRepositoryInfo, branch?: BranchSchema): Promise<void> {
    for (const commit of newCommits) {
        if (
            // Filter those containing card short id
            commit.message.match(cardRegex) &&
            // Filter unique commits
            !commits.value.some((c) => c.id === commit.id)
        ) {
            let refs = branch?.name;
            if (!refs) {
                // Request references branches and tags
                const api = getGitlabApi(repoInfo);
                refs = (await api.Commits.allReferences(repoInfo.repoId, commit.id)).map(ref => ref.name).join(', ');
            }
            commits.value.push({
                ...commit,
                // Add branch name and repo alias info
                ...{ branchName: refs, repoName: repoInfo.repoName },
            });
        }
    }
}

/**
 * Reset to page 1 and refresh commits to restart from 0 
 */
async function refresh() {
    isLoading.value = true;
    page = 1;
    branchesByRepo.value = [];
    commits.value = [];
    branches.value = [];
    mergeRequests.value = [];
    await fetchBranches();
    await Promise.all([fetchMergeRequests(), fetchCommits()]);
    isLoading.value = false;
}

/**
 * Fetch commits from next page
 */
function nextPage() {
    page++;
    fetchCommits();
}
</script>

<template>
    <template v-if="isLoading">
        Loading ...
    </template>

    <template v-else-if="!repoInfos?.length">
        Missing Gitlab configuration
    </template>

    <template v-else>
        <div class="header">
            <h3>Branches</h3>
            <button @click="refresh()" type="button" class="refresh mod-primary"
                title="Clear tables and rescan repositories">
                Refresh
            </button>
        </div>
        <ul v-if="branches.length">
            <li v-for="branch in branches">
                {{ branch.repoName }} :
                <a target="_blank" :href="branch.web_url">
                    {{ branch.name }}
                </a>
            </li>
        </ul>
        <template v-else>No corresponding branches found</template>
        <hr />

        <h3>Merge Requests</h3>
        <table v-if="mergeRequests.length">
            <thead>
                <tr>
                    <th>Merge Request</th>
                    <th>State</th>
                    <th>Merge state</th>
                    <th title="True if at least one person approved">Approved</th>
                    <th title="Unresolved notes/comments">Unresolved</th>
                    <th title="Number of commits behind target branch">Behind target</th>
                    <th>Repo</th>
                </tr>
            </thead>
            <tbody id="commit-list">
                <tr v-for="mergeRequest in mergeRequests">
                    <td><a target="_blank" :href="mergeRequest.web_url">{{ mergeRequest.title }}</a></td>
                    <td>{{ mergeRequest.state }}</td>
                    <td>{{ mergeRequest.detailed_merge_status }}</td>
                    <td>{{ mergeRequest.approved }}</td>
                    <td>
                        <template v-for="note in mergeRequest.notes">
                            <a target="_blank" :href="note.url">{{ note.id }}</a><br />
                        </template>
                    </td>
                    <td>{{ mergeRequest.nbCommitsBehindTarget }}</td>
                    <td>{{ mergeRequest.repoName }}</td>
                </tr>
            </tbody>
        </table>
        <template v-else>No corresponding merge requests found</template>
        <hr />

        <div class="header">
            <h3>Commits</h3>
            <button @click="nextPage()" type="button" class="next-page" :title="`Scan ${data.perPage} more commits`">
                Next page
            </button>
        </div>
        <table v-if="commits.length">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Commit</th>
                    <th>Branch</th>
                    <th>Repo</th>
                </tr>
            </thead>
            <tbody id="commit-list">
                <tr v-for="commit in commits">
                    <td>{{ new Date(commit.created_at).toLocaleString() }}</td>
                    <td>{{ commit.author_name }}</td>
                    <td><a target="_blank" :href="commit.web_url">{{ commit.title }}</a></td>
                    <td>{{ commit.branchName }}</td>
                    <td>{{ commit.repoName }}</td>
                </tr>
            </tbody>
        </table>
        <template v-else>No corresponding commits found</template>
    </template>
</template>

<style lang="scss" scoped>
.header {
    display: flex;
    justify-content: space-between;
    align-items: baseline;
    margin-bottom: 5px;
}
</style>