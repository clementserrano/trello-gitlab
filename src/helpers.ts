import { BranchSchema, Gitlab, DiscussionNoteSchema, MergeRequestSchema } from "@gitbeaker/rest";

/**
 * Type for merge request with all its information
 */
export type MergeRequestInfo = (MergeRequestSchema
    & { 
        repoName: string, 
        approved: boolean | null, 
        notes: (DiscussionNoteSchema & { url: string })[],
        nbCommitsBehindTarget: number | null
    });

/**
 * Interface for repository information stored in Trello data
 */
export interface GitlabRepositoryInfo {
    apiUrl: string;
    repoName: string;
    repoId: string;
    accessToken: string;
}

/**
 * Interface to group Gitlab resources by repository
 */
export interface BranchesByRepo {
    repoInfo: GitlabRepositoryInfo;
    branches: BranchSchema[];
}

/**
 * Interface for Trello data
 */
export interface TrelloData {
    /**
     * List of repositories
     */
    repoInfos: GitlabRepositoryInfo[];
    /**
     * Number of commits to fetch by page
     */
    perPage: number;
    /**
     * Regex to filter branches and commits depending on trello card id
     */
    regex: string;
}

/**
 * Data id to identify app data in Trello
 */
export const dataId = "gitlab-config";

/**
 * Data field to identify field to get from Trello data
 */
export const dataField = "data";

/**
 * Identifier for trello card id in regex
 */
export const regexCardId = "${id}";

type dataScope = 'board' | 'card';
type dataAccess = 'shared' | 'private' | 'public';

/**
 * Trello Power up library
 */
export const trello: {
    /**
     * See [Trello building power-up guide](https://developer.atlassian.com/cloud/trello/guides/power-ups/building-a-power-up-part-one/#adding-functionality) for details
     * Only called once on connector page to declare all power-up functionnalities
     */
    initialize: Function
    /**
     * Called in functionnality page (card, settings) to access context
     * @returns 
     */
    iframe: () => {
        /**
         * Get Trello data
         * @param scope context scope
         * @param access context access
         * @param id data id to identify data for this power-up
         * @returns trello data
         */
        get: (scope: dataScope, access: dataAccess, id: string) => Promise<TrelloData>,

        /**
         * Set Trello data
         * @param scope context scope
         * @param access context access
         * @param id data id to identify data for this power-up
         * @param data new trello data
         */
        set: (scope: dataScope, access: dataAccess, id: string, data: TrelloData) => Promise<void>,

        /**
         * Return value of property in card
         * @param property card property 
         * @returns value
         */
        card: (property: string) => Promise<any>
    }
} = window.TrelloPowerUp;

/**
 * Get data from Trello board
 * @returns trello data
 */
export async function getTrelloData(): Promise<TrelloData> {
    return await trello.iframe().get("board", "shared", dataId);
}

/**
 * Set data on Trello board
 */
export async function setTrelloData(data: TrelloData): Promise<void> {
    await trello.iframe().set("board", "shared", dataId, data);
}

/**
 * Return Gitlab object corresponding to repository info from Trello data
 * @param repoInfo info from Trello data
 * @returns Gitlab API object, to make api calls from
 */
export function getGitlabApi(repoInfo: GitlabRepositoryInfo) {
    return new Gitlab({
        host: repoInfo.apiUrl,
        token: repoInfo.accessToken,
    });
}