import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'

const routes = [
  { path: '/', component: () => import('./components/Connector.vue') },
  { path: '/card-details', component: () => import('./components/CardDetails.vue') },
  { path: '/settings', component: () => import('./components/Settings.vue') },
  { path: '/about', component: () => import('./components/About.vue') }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

createApp(App).use(router).mount('#app');